import org.testng.annotations.Test;

public class HomeGoogleTest extends BaseTest {

    @Test
    public void buscarEnGoogle() throws Exception {
        HomeGoogle homeGoogle = new HomeGoogle(driver);
        homeGoogle.realizarBusqueda("Tsoft");

        generarLog(true,"Busqueda por Google");
    }
}
