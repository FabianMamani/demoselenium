import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.util.concurrent.TimeUnit;

public class HomeGoogle extends BasePage {
    private By buscador =By.xpath("/html[1]/body[1]/div[2]/div[2]/form[1]/div[2]/div[1]/div[1]/div[1]/div[2]/input[1]");
    private By linkTsoft = By.xpath("//*[@id=\"rso\"]/div[1]/div/div/div[1]/a/h3/span");


    HomeGoogle(WebDriver driver) {
        super(driver);
        driver.get("http://google.com");
    }

    public void realizarBusqueda(String elemento) throws InterruptedException {
        driver.findElement(buscador).sendKeys(elemento);
        driver.findElement(buscador).submit();
        Thread.sleep(3000);
        driver.findElement(linkTsoft).click();
        Thread.sleep(3000);
    }
}
